var submit = document.querySelector("#submit"),
    fName = document.querySelector("#firstName"),
    lName = document.querySelector("#lastName"),
    Position = document.querySelector("#position"),
    Company = document.querySelector("#company"),
    Type = document.querySelector("#type"),
    Country = document.querySelector("#country"),
    Email = document.querySelector("#email"),
    yes = document.querySelector('#yesBox'),
    no = document.querySelector('#noBox')
    errors = 0;


function createErrorTag(msg) {
	var span = document.createElement("span");
	span.innerText = msg;
	span.classList.add("error");
	return span;
}

function removeErrors() {
	var errors = document.querySelectorAll(".error");
	for(var i = 0; i < errors.length; i++) {
		errors[i].parentElement.removeChild(errors[i]);
    }
}

function validateName() {
	var regex = /^([a-z]+\s?[a-z]+)$/gi;
	if(!regex.test(fName.value)) {
		errors++;
		var span = createErrorTag("Please enter your first name.");
		document.querySelector(".named").appendChild(span);
	}
}

function validateLastName() {
	var regex = /^([a-z]+\s?[a-z]+)$/gi;
	if(!regex.test(lName.value)) {
		errors++;
		var span = createErrorTag("Please enter your last name.");
		document.querySelector(".lastNamed").appendChild(span);
	}
}

function validatePosition() {
	if(position.value == "") {
		errors++;
		var span = createErrorTag("Please enter your job title.");
		document.querySelector(".positioned").appendChild(span);
	}
}

function validateCompany() {
	if(company.value == "") {
		errors++;
		var span = createErrorTag("Please Enter the name of your company/employer.");
		document.querySelector(".companied").appendChild(span);
	}
}

function validateType(){
    if(type.selectedIndex == "") { 
    errors++;
    var span = createErrorTag("Please Select the type of the company.");
    document.querySelector(".typed").appendChild(span);
    }
}

function validateCountry(){
    if(Country.selectedIndex == "") { 
    errors++;
    var span = createErrorTag("Please Select your country.");
    document.querySelector(".countried").appendChild(span);
    }
}

function validateEmail() {
	var regex = /^([a-z0-9]+@[a-z]+(\.[a-z]+)?\.[a-z]+)$/gi;
	if(!regex.test(Email.value)) {
		errors++;
		var span = createErrorTag("Please Enter your work email address.");
		document.querySelector(".emaild").appendChild(span);	
	}
}

function validateCheck(){
    if ( ( yes.checked == false ) && ( no.checked == false ) ){ 
    errors++;
    var span = createErrorTag("Please Select Yes or No.");
    document.querySelector(".options").appendChild(span);
    }
}

submit.addEventListener("click", function(event) {
    event.preventDefault();
	removeErrors();
    validateName();
    validateLastName();
    validatePosition();
    validateCompany();
    validateEmail();
    validateCheck();
    validateType();
    validateCountry();
	if(errors) {
		errors = 0;
		return false;
	}
		return true;
});

// back to top using jquery
$(document).ready(function(){
    var backTop = $('.to-top a');
    $(window).scroll(function () {

        if ($(this).scrollTop() > 450) {
            $(backTop).fadeIn();
        } else {
            $(backTop).fadeOut();
        }
    });

    $(backTop).click(function () {
        $("html, body").animate({ scrollTop: 0 }, 1000);
    });

    // hamburger functionality added
     //  hamburger styling done
     var hamburger = $('.ham')
     var mobNav = $('nav ul')
     $(hamburger).click(function () {
         $(this).toggleClass("open");
         $(mobNav).toggle();
     })         

})